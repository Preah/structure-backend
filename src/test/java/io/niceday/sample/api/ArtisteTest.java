package io.niceday.sample.api;

import io.niceday.common.engine.test.SuperTest;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.transaction.annotation.Transactional;

import static io.niceday.sample.api.ArtistHelper.*;

/**
 * @since       20120.02.14
 * @author      preah
 * @description artist test
 **********************************************************************************************************************/
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
public class ArtisteTest extends SuperTest {

	@Test
	public void t01_getPage() {
		add    (addArtist());
		add    (addArtist());
		getPage(findArtist());
	}

	@Test
	public void t02_get() {
		get(add(addArtist()));
	}

	@Test
	public void t03_add() {
		add(addArtist());
	}

	@Test
	public void t04_modify() {
		modify(add(addArtist()), modifyArtist());
	}

	@Test
	public void t05_remove() {
		remove(add(addArtist()));
	}






}