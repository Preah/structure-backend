package io.niceday.common.engine.aop;


import io.niceday.common.engine.exception.SQLErrorException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import io.niceday.common.engine.exception.GoneException;
import io.niceday.common.engine.exception.NotAcceptableException;

import java.sql.SQLException;

/**  
 * @since       2018.10.03
 * @author      lucas
 * @description exception handler(reference site : http://onecellboy.tistory.com/346)
 **********************************************************************************************************************/
@RestControllerAdvice
public class ExceptionsHandler {

	@ExceptionHandler({SQLErrorException.class, SQLException.class})
	public ResponseEntity<?> handleSQLErrorException(RuntimeException e){
		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(NotAcceptableException.class)
	public ResponseEntity<?> handleNotAcceptableException(NotAcceptableException exception){
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_ACCEPTABLE);
	}

	@ExceptionHandler(GoneException.class)
	public ResponseEntity<?> handleGoneException(GoneException exception){
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.GONE);
	}
}
