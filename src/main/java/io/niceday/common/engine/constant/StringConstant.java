package io.niceday.common.engine.constant;

/**
* @since       2019.04.15
* @author      lucas
* @description string constant
**********************************************************************************************************************/
public class StringConstant {

    public final static String EMPTY            = "";
    public final static String BLANK            = " ";
    public final static String ASTERISK         = "*";
    public final static String COMMA            = ",";
    public final static String SEMICOLON        = ";";
    public final static String DOT              = ".";
    public final static String SLASH            = "/";
    public final static String DOUBLE_QUOTATION = "\"";
}
