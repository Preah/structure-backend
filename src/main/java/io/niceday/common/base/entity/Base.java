package io.niceday.common.base.entity;

import java.time.LocalDateTime;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Getter;
import lombok.Setter;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description base
 **********************************************************************************************************************/
@Getter
@Setter
public abstract class Base {

	@CreatedDate
	private LocalDateTime createdAt;
	
	@LastModifiedDate
	private LocalDateTime updatedAt;
}
