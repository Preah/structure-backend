package io.niceday.common.base.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @since       2020.02.12
 * @author      preah
 * @description page
 **********************************************************************************************************************/

public class PageDefault {

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class Search {

		@ApiModelProperty(value="페이지번호")
		private Integer pageNo = 0;

		@ApiModelProperty(value="페이지사이즈")
		private Integer pageSize = 10;
	}
}
