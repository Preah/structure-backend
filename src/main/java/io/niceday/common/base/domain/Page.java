package io.niceday.common.base.domain;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @since       2020.02.12
 * @author      preah
 * @description page
 **********************************************************************************************************************/

@Data
@NoArgsConstructor
@Builder(toBuilder = true)
public class Page<T> {

	private Integer pageNo;
	private Integer pageSize;
	private Integer totalCount;
	private List<T> contents;


	public Page(Integer pageNo, Integer pageSize, Integer totalCount, List<T> contents) {
		this.pageNo     = ObjectUtils.isEmpty(pageNo)     ?  0 : pageNo;
		this.pageSize   = ObjectUtils.isEmpty(pageSize)   ? 10 : pageSize;
		this.totalCount = ObjectUtils.isEmpty(totalCount) ?  0 : totalCount;
		this.contents   = ObjectUtils.isEmpty(contents)   ? Collections.emptyList() : contents;
	}

	public Page map(Function<? super T, ? extends T> function) {

		this.contents = ObjectUtils.isEmpty(this.contents) ? Collections.emptyList() : this.contents.stream().map(function::apply).collect(Collectors.toList());
		return this;
	}
}
