package io.niceday.sample.api.form;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.niceday.common.base.domain.PageDefault;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**   
 * @since       2020.02.11
 * @author      artist
 * @description preah form
 **********************************************************************************************************************/
public class ArtistForm {

	public static class Request {

		@Getter
		@Setter
		@Builder
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
		public static class Find extends PageDefault.Search {

			@ApiModelProperty(value="아티스트일련번호")
			private Integer artistId;
		}

		@Getter
		@Setter
		@Builder
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Add {

			@ApiModelProperty(value="이름")
			@NotBlank
			@Length(max=100)
			private String name;

			@ApiModelProperty(value="등록자")
			@Length(max=200)
			private String creator;
		}

		@Getter
		@Setter
		@Builder
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Modify {

			@ApiModelProperty(value="이름")
			@NotBlank
			@Length(max=100)
			private String name;

			@ApiModelProperty(value="등록자")
			@Length(max=200)
			private String creator;
		}
	}

	public static class Response {

		@Data
		public static class FindAll {

			@ApiModelProperty(value="아티스트일련번호")
			private Integer artistId;

			@ApiModelProperty(value="이름")
			private String name;

			@ApiModelProperty(value="등록자")
			private String creator;

			List<Album> albums;
		}

		@Data
		public static class FindOne {

			@ApiModelProperty(value="아티스트일련번호")
			private Integer artistId;

			@ApiModelProperty(value="이름")
			private String name;

			@ApiModelProperty(value="등록자")
			private String creator;

			List<Album> albums;
		}

		@Data
		public static class Album {

			private Integer albumId;
			private String  title;
			private String  creator;
		}
	}
}
