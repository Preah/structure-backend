package io.niceday.sample.api.service;

import io.niceday.common.base.domain.Page;
import io.niceday.sample.api.entity.Artist;
import io.niceday.sample.api.repository.ArtistRepository;
import io.niceday.sample.api.form.ArtistForm.Request.Find;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import static io.niceday.common.engine.helper.model.ObjectHelper.toJson;
import static io.niceday.sample.api.mapper.ArtistMapper.mapper;

/**   
 * @since       2020.02.11
 * @author      preah
 * @description artist service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class ArtistService {

	private final ArtistRepository artistRepository;

	@Transactional(readOnly=true)
	public Page getPage(Find find) {
		Page page = artistRepository.getPage(toJson(find));
		return ObjectUtils.isEmpty(page) ? mapper.toPage(find) : page;
	}

	@Transactional(readOnly=true)
	public Artist get(Integer artistId) {
		return artistRepository.get(toJson(Artist.builder().artistId(artistId).build()));
	}

	public Integer add(Artist artist) {
		return artistRepository.add(toJson(artist));
	}

	public Integer modify(Artist artist) {
		return artistRepository.modify(toJson(artist));
	}

	public void remove(Integer artistId) {
		artistRepository.remove(toJson(Artist.builder().artistId(artistId).build()));
	}
}