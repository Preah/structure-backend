package io.niceday.sample.api.mapper;

import io.niceday.common.base.domain.Page;
import io.niceday.sample.api.entity.Artist;
import io.niceday.sample.api.form.ArtistForm.Request.Find;
import io.niceday.sample.api.form.ArtistForm.Request.Add;
import io.niceday.sample.api.form.ArtistForm.Request.Modify;
import io.niceday.sample.api.form.ArtistForm.Response.FindAll;
import io.niceday.sample.api.form.ArtistForm.Response.FindOne;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**   
 * @since       2020.02.11
 * @author      preah
 * @description artist mapper
 **********************************************************************************************************************/
@Mapper(unmappedTargetPolicy=ReportingPolicy.IGNORE)
public interface ArtistMapper<t> {

    ArtistMapper mapper = Mappers.getMapper(ArtistMapper.class);

    Page    toPage(Find form);
    FindOne toFindOne(Artist entity);
    FindAll toFindAll(Artist entity);

    Artist  toArtist(Add form);
    Artist  toArtist(Integer artistId, Modify form);
}