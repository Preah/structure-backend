package io.niceday.sample.api.controller;

import io.niceday.common.base.domain.Page;
import io.niceday.sample.api.entity.Artist;
import io.niceday.sample.api.form.ArtistForm.Request.Add;
import io.niceday.sample.api.form.ArtistForm.Request.Find;
import io.niceday.sample.api.form.ArtistForm.Request.Modify;
import io.niceday.sample.api.form.ArtistForm.Response.FindAll;
import io.niceday.sample.api.form.ArtistForm.Response.FindOne;
import io.niceday.sample.api.service.ArtistService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.function.Function;

import static io.niceday.sample.api.mapper.ArtistMapper.mapper;

/**
 * @since       2020.02.11
 * @author      preah
 * @description artist controller
 **********************************************************************************************************************/
@Api(description="아티스트-샘플")
@RestController
@RequiredArgsConstructor
@RequestMapping("${property.api.end-point}")
public class ArtistController {

    private final ArtistService artistService;

    @ApiOperation("페이지")
    @GetMapping("/artists/pages")
    public Page<FindAll> getPage(@Valid Find find) {
        return artistService.getPage(find).map((Function<Artist, FindAll>)mapper::toFindAll);
    }

    @ApiOperation("조회")
    @GetMapping("/artists/{artistId}")
    public FindOne get(@PathVariable Integer artistId){
        return mapper.toFindOne(artistService.get(artistId));
    }

    @ApiOperation("등록")
    @PostMapping("/artists")
    public Integer add(@Valid @RequestBody Add add){
        return artistService.add(mapper.toArtist(add));
    }

    @ApiOperation("수정")
    @PutMapping("/artists/{artistId}")
    public Integer modify(@PathVariable Integer artistId, @Valid @RequestBody Modify modify){
        return artistService.modify(mapper.toArtist(artistId, modify));
    }

    @ApiOperation("삭제")
    @DeleteMapping("/artists/{artistId}")
    public void remove(@PathVariable Integer artistId){
        artistService.remove(artistId);
    }
}