package io.niceday.sample.api.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class Song {

    private Integer songId;
    private String  name;
    private String  creator;

    private Album album;
}
