package io.niceday.sample.api.repository;

import io.niceday.common.base.domain.Page;
import io.niceday.sample.api.entity.Artist;
import org.apache.ibatis.annotations.Mapper;

/**
 * @since       2020.02.11
 * @author      preah
 * @description artist repository
 **********************************************************************************************************************/
@Mapper
public interface ArtistRepository {

	Artist  get(String artist);
	Integer add(String artist);
	Integer modify(String artist);
	void    remove(String artist);

	Page    getPage(String find);
}
