# CMS ADMIN API
  
  ## 패키지 구조   
  | 패키지      | 설명        |
  | --------  |  --------  |
  | kr.co.genie.cms.common             | 공통 패키지(비니지스 로직이 포함되지 않는 상위 영역 ex. aop, config..) |
  | kr.co.genie.cms.platform.sample    | 샘플 패키지(기본 CRUD)|
  | kr.co.genie.cms.platform.common    | 비지니스 공통 영역|
  | kr.co.genie.cms.platform.access    | 접근 제어 영역|
  | kr.co.genie.cms.platform.alone     | 독립적 실행 영역|
  | kr.co.genie.cms.platform.assist    | 비지니스 지원 영역|
  | kr.co.genie.cms.platform.basis     | 비지니스 메인 영역|
  
  ## 기능별 패키지 구조   
     - api     : /api/platform
  | 패키지      | 엔티티      | API        |  설명 |
  | --------  |  --------  |  --------  |  --------  |                
  | kr.co.genie.platform.basis.program                      |Program                    |/program                                              |프로그램|					
  | kr.co.genie.platform.basis.template                     |Template	                |/template                                             |템플릿|					
  | kr.co.genie.platform.basis.page                         |Page	                |/page                                                 |페이지|					
  | kr.co.genie.platform.assist.body                        |Body		        |/body                                                 |바디|				
  | kr.co.genie.platform.assist.line                        |Line		        |/line                                                 |공통-유무선|			
  | kr.co.genie.platform.alone.layout.background.color      |BackgroundColor	        |/layout/background-color                              |레이아웃-백그라운드-컬러|			
  | kr.co.genie.platform.alone.layout.background.image      |BackgroundImage	        |/layout/background-image                              |레이아웃-백그라운드-이미지|			
  | kr.co.genie.platform.alone.layout.font                  |Font			|/layout/font                                          |레이아웃-폰트|		
  | kr.co.genie.platform.alone.component.menu               |MenuPool                   |/component/menu-pool                                  |메뉴|	
  | kr.co.genie.platform.alone.component.menu               |Item	                |/component/menu-pool/item                             |메뉴-아이템|	
  | kr.co.genie.platform.alone.component.movie              |MoviePool		        |/component/movie-general-pool                         |컴포넌트-영상-묶음영상|			
  | kr.co.genie.platform.alone.component.movie              |Movie			|/component/movie-general-pool/general                 |컴포넌트-영상-묶음영상-기본|
  | kr.co.genie.platform.alone.component.movie              |MovieMainSubPool	        |/component/movie-main-sub-pool                        |컴포넌트-영상-묶음영상|			
  | kr.co.genie.platform.alone.component.movie              |MovieMainSub	        |/component/movie-main-sub-pool/main-sub               |컴포넌트-영상-묶음영상-메인/서브|
  | kr.co.genie.platform.alone.component.album              |AlbumPool		        |/component/album-pool                                 |컴포넌트-앨범-묶음앨범|			
  | kr.co.genie.platform.alone.component.album              |Album			|/component/album-pool/album                           |컴포넌트-앨범-묶음앨범-앨범|		
  | kr.co.genie.platform.alone.component.song               |SongPool		        |/component/song-pool                                  |컴포넌트-음원-묶음음원|			
  | kr.co.genie.platform.alone.component.song               |Song			|/component/song-pool/song                             |컴포넌트-음원-묶음음원-기본|	
  | kr.co.genie.platform.alone.component.song               |SongAlbumPool	        |/component/song-album-pool                            |컴포넌트-음원-묶음앨범음원|			
  | kr.co.genie.platform.alone.component.song               |SongAlbum		        |/component/song-album-pool/song-album                 |컴포넌트-음원-묶음앨범음원-앨범음원|		
  | kr.co.genie.platform.alone.component.main               |MainPool		        |/component/main-general-pool                          |컴포넌트-메인이미지-묶음메인이미지|			
  | kr.co.genie.platform.alone.component.main               |Main			|/component/main-general-pool/general                  |컴포넌트-메인이미지-묶음메인이미지-기본|
  | kr.co.genie.platform.alone.component.main               |MainSlidePool		|/component/main-slide-pool                            |컴포넌트-메인이미지-묶음메인이미지|			
  | kr.co.genie.platform.alone.component.main               |MainSlide			|/component/main-slide-pool/slide                      |컴포넌트-메인이미지-묶음메인이미지-슬라이드|
  | kr.co.genie.platform.alone.component.image              |ImagePool		        |/component/image-pool                                 |컴포넌트-이미지-묶음이미지|
  | kr.co.genie.platform.alone.component.image              |Image			|/component/image-pool/image                           |컴포넌트-이미지-묶음이미지-기본|
  | kr.co.genie.platform.alone.component.image              |ImagePositionPool		|/component/image-position-pool                        |컴포넌트-이미지-묶음이미지|			
  | kr.co.genie.platform.alone.component.image              |ImagePosition		|/component/image-position-pool/position               |컴포넌트-이미지-묶음이미지-노출이미지|
  | kr.co.genie.platform.alone.component.lineup             |LineupPool		        |/component/lineup-pool                                |컴포넌트-라인업-묶음라인업|			
  | kr.co.genie.platform.alone.component.lineup             |Lineup			|/component/lineup-pool/lineup                         |컴포넌트-라인업-묶음라인업-라인업|		
  | kr.co.genie.platform.alone.component.photo              |PhotoPool		        |/component/photo-pool                                 |컴포넌트-포토-묶음포토|			
  | kr.co.genie.platform.alone.component.photo              |Photo			|/component/photo-pool/photo                           |컴포넌트-포토-묶음포토-포토|		
  | kr.co.genie.platform.alone.component.popup              |PopupPool		        |/component/popup-pool                                 |컴포넌트-팝업-묶음팝업|			
  | kr.co.genie.platform.alone.component.popup              |Popup			|/component/popup-pool/pupop                           |컴포넌트-팝업-묶음팝업-팝업|		
  | kr.co.genie.platform.alone.component.tab                |TabPool		        |/component/tab-pool                                   |컴포넌트-탭-묶음탭|			
  | kr.co.genie.platform.alone.component.tab                |Tab			|/component/tab-pool/tab                               |컴포넌트-탭-묶음탭-탭|		
  | kr.co.genie.platform.alone.component.tab                |TabRow			|/component/tab-pool/tab/row                           |컴포넌트-탭-묶음탭-탭-행|	
  | kr.co.genie.platform.alone.component.tab                |TabColumn			|/component/tab-pool/tab/row/column                    |컴포넌트-탭-묶음탭-탭-행-열|
  | kr.co.genie.platform.alone.component.board              |BoardPool		        |/component/board-general-pool                         |컴포넌트-게시판-묶음게시판|			
  | kr.co.genie.platform.alone.component.board              |Board			|/component/board-general-pool/general                 |컴포넌트-게시판-묶음게시판-일반|
  | kr.co.genie.platform.alone.component.board              |BoardImage			|/component/board-general-pool/general/image           |컴포넌트-게시판-묶음게시판-일반-이미지|
  | kr.co.genie.platform.alone.component.board              |BoardManagePool		|/component/board-manage-pool                          |컴포넌트-게시판-묶음게시판|			
  | kr.co.genie.platform.alone.component.board              |BoardManage		|/component/board-manage-pool/manage                   |컴포넌트-게시판-묶음게시판-운영|
  | kr.co.genie.platform.alone.component.board              |BoardManageImage		|/component/board-manage-pool/manage/image             |컴포넌트-게시판-묶음게시판-운영-이미지|	
  | kr.co.genie.platform.alone.component.comment            |CommentPool		|/component/comment-pool                               |컴포넌트-댓글-묶음댓글|			
  | kr.co.genie.platform.alone.component.text               |TextPool		        |/component/text-pool                                  |컴포넌트-묶음-묶음텍스트|			
             
  ## 기능별 공통 패키지 구조
  | 패키지 | 설명 |
  |  --------  |  --------  |
  | kr.co.genie.cms.platform.sample.entity     |엔티티 영역(Entity)|
  | kr.co.genie.cms.platform.sample.form       |폼 영역(Dto)|
  | kr.co.genie.cms.platform.sample.enumerate  |열거형 영역(Enum)|
  | kr.co.genie.cms.platform.sample.mapper     |매퍼 영역(Mapper)|
  | kr.co.genie.cms.platform.sample.controller |컨트롤러 영역(Controller)|
  | kr.co.genie.cms.platform.sample.service    |서비스 영역(Service)|
  | kr.co.genie.cms.platform.sample.repository |레파지토리 영역(Repository)|

  ## 기능별 공통 테스트 패키지 구조
  | 패키지 | 설명 |
  |  --------  |  --------                                        |
  | kr.co.genie.cms.platform.sample   | 테스트 케이스 영역(Test, Helper) |
  
  ## REST API 디자인
  | 기능          | 함수명       | API                                      | 
  |  --------    |  --------  | --------                                 |
  | 페이지 목록     | getPage    | GET    /api/samples/pages                | 
  | 목록          | getAll     | GET    /api/samples                       | 
  | 조건 목록      | getAll     | GET    /api/samples/likes/title/content   | 
  | 관계 목록      | getAll     | GET    /api/samples/{샘플아이디}/comments  |
  | 조회          | get        | GET    /api/samples/{샘플아이디}           | 
  | 등록          | add        | POST   /api/samples                       |
  | 수정          | modify     | PUT    /api/samples/{샘플아이디}           | 
  | 삭제          | remove     | DELETE /api/samples/{샘플아이디}           | 
  | 존재 여부      | exists     | HEAD   /api/samples/{샘플아이디}           |
  
  * 페이지 : 마지막 uri에 pages작성
  * 목록 : 복수로 작성
  * 조건 목록 : 복수 뒤에 likes 를 붙이고 이하로 조건들을 작성
  * 관계 목록 : 샘플의 댓글목록을 가져오는 것이다. 이 처럼 도메인의 구조에 맞춰 데이터를 가져올 경우 이와 같이 상하 구조대로 api를 디자인 한다.
  * 등록 : 복수로 작성
  * 수정 : 복수 뒤에 아이디
  * 삭제 : 복수 뒤에 아이디 
  * 존재 여부 : 복수 뒤에 아이디
